export default [
  {
    route: '/main',
    text: 'Главная',
  },
  {
    route: '/carusel',
    text: 'Карусель',
  },
  {
    route: '/second-task',
    text: 'Второе задание',
  },
]