import React, { useState } from 'react';
import block from 'bem-cn';

import imagies from './img';

import './Carusel.scss';

const Carusel = () => {
  const [tempImageIndex, changeIndex] = useState(0);
  const b = block('carusel');
  return <section className={b()}>
    <div
      className={b('button')}
      onClick={() => changeIndex(tempImageIndex === 0 ? 0 : tempImageIndex - 1)}>
        {'<'}
      </div>
    <img src={imagies[tempImageIndex]} className={b('image')} alt="" />
    <div
      className={b('button')}
      onClick={() => changeIndex(tempImageIndex === imagies.length - 1 ? imagies.length - 1 : tempImageIndex + 1)}>
        >
      </div>
  </section>
}

export default Carusel;