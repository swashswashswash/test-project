import React from 'react';
import block from 'bem-cn';
import { useLocation} from 'react-router';
import { Link } from 'react-router-dom';

import items from './data';

import './Header.scss';

const Header = () => {
  const b = block('header');
  const location = useLocation();
  const itemsLest = items.map(tempRoute => <Link
    to={tempRoute.route}
    key={tempRoute.route}
    className={b('menu-item', {active: location.pathname === tempRoute.route})}>
    {tempRoute.text}
  </Link>)
  return <header className={b()}>
    <nav className={b('menu')}>
      {itemsLest}
    </nav>
  </header>;
}

export default Header;