import firstImage from './first.jpg';
import secondImage from './second.jpg';
import thirdImage from './third.jpg';

export default [
  firstImage,
  secondImage,
  thirdImage
]