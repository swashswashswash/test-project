import React from 'react';
import block from 'bem-cn';
import { Switch, Route } from 'react-router-dom';
import './App.scss';

import Header from './components/Header';
import Main from './components/Main';
import Calculator from './components/Calculator';
import Carusel from './components/Carusel';

function App() {
  const b = block('app');
  return (
    <div className={b()}>
      <Header />
      <main className={b('main')}>
        <Switch>
          <Route path='/main' component={Main} />
          <Route path='/carusel' component={Carusel} />
          <Route path='/second-task' component={Calculator} />
        </Switch>
      </main>
    </div>
  );
}

export default App;
