import React from 'react';
import block from 'bem-cn';

import './Main.scss';

const Main = () => {
  const b = block('main');
  return <section className={b()}>
    Тут нет ничего интересного
  </section>
}

export default Main;