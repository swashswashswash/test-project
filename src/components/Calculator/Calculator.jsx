import React, { useState } from 'react';
import block from 'bem-cn';

import './Calculator.scss';

const Calculator = () => {
  const [stroke, changeStroke] = useState('');
  const b = block('calculator');
  const longWord = stroke.split(' ').sort((a, b) => b.length - a.length);
  return <section className={b()}>
    <input
      className={b('input')}
      value={stroke}
      placeholder="Введите слова через пробел, и я  найду самое длинное слово"
      onChange={e => changeStroke(e.currentTarget.value)} />
    {longWord[0].length !== 0 && <div className={b()}>{`Самое длинное слово - ${longWord[0]}`}</div>}
  </section>
}

export default Calculator;